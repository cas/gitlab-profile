**Collective Adaptive Systems** refer to a form of complex systems where a large number of heterogeneous entities interact without specific external or internal central control, adapt their behaviour to environmental settings in pursuit of an individual or collective goal. Actual behaviour arises as an emergent property through swarm or collective intelligence. Examples include understanding emergence and social behaviour of natural life (e.g. bacteria self-organising to overcome shortage of food),  engineering swarm robotics, developing socio-technical systems  and more generally developing services  for smart and sustainable cities.

**We lead and develop research in three main areas:**

- Studying natural systems (e.g. biological, social, human ones) and identifying essential models,  mechanisms and interactions at work at the heart of those systems, mostly through agent-based models, simulations and design patterns.
- Designing and developing artificial collective adaptive systems and different forms of emergent behaviour  (e.g. swarm robotics, ecosystems of spatial services for smart cities, higher-order emergence) 
- Verifying the reliability and trustworthiness of those systems prior to their deployment in real-life settings.

**Since several years we also investigate Semantic AI:**

- semantic-based multi-agents systems for service composition;
- semantic-based analysis of geo-spatial application data and interoperability issues;
- artificial intelligence techniques for geo-spatial applications.
 

Recently we started new research aiming at developing innovative services in the academic field. We experiment various innovations leveraging students projects, set up an [**accelerator of science and services**](https://www.unige.ch/stic/innovation) to devise prototypes, as well as an R&D unit to continue the pipeline of innovation into proper industrialization of innovative services. We are pioneer in Switzerland in this field for the academic sector. We complete our hands-on experiments with building up of an academic community through workshops and hackathons.

Watch videos [here](https://www.unige.ch/cui/cas/publications/videos).